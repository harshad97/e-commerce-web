//Dependencies
import React, { Component } from "react";
//Internals
import AllItems from "./AllItems";

import "./index.css";
import Button from "react-materialize/lib/Button";

class Products extends Component {
  constructor(props){
    super(props)
    this.state ={
      valid : localStorage.getItem('auth')
    }
  }
  handleLogout = () => {
    localStorage.setItem('auth',"false")
    this.setState({
      valid : "false"
    })
  }
  render() {
    return (
      <div className="items-wrapper">
        {console.log(this.state.valid)} 
        <div className="items-title">
          <h4>All Items</h4>
          <div>
             {this.state.valid === "true" ? <div  style={{margin:20,display:'flex',justifyContent:'flex-end'}}>
               <span style={{marginRight:20}}><h6>User : {localStorage.getItem('name')}</h6></span>
               <span><Button onClick={this.handleLogout }>Logout</Button></span>
             </div> : ""}
          </div>
        </div>
        <AllItems />
      </div>
    );
  }
}

export default Products;
