//Dependencies
import React, { Component } from 'react';
import { Icon } from 'react-materialize';
import { Link } from 'react-router-dom';
import map from 'lodash/map';

class AllItems extends Component{
  constructor(props){
    super(props)
    this.state ={
      loading :true,
      product : null 
    }
  }
  render(){
    const {loading,product} = this.state
    return <div className="items">
    {loading 
    ? 
       <p>Loading...</p> 
    :
        map(product, (product)=> (
      <div key={product.id} className="item">
        {/* <Link to={`/products/${product._id}`}> */}
        <div className="product-img">
          <img alt={product.name} src={product.img} />
        </div>
        <div className="product-details">
          <h1 id="product-name">{product.name}</h1>
          <h4 id="product-description">{product.description}</h4>
        </div>
        {/* </Link> */}
        <div className="price-add">
          <h5 id="product-price">${product.price}</h5>
          <Icon small id="add-icon">add_shopping_cart</Icon>
        </div>
      </div>
    ))}
  </div>
  }
  componentDidMount() {
    fetch("http://localhost:4000/category/getcategory")
    .then(res => res.json())
    .then(product => {
      this.setState({
        product,
        loading:false
      })
    })
  }
}



export default AllItems;
