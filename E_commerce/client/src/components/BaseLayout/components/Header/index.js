//Dependencies
import React from 'react';
import {Link} from 'react-router-dom';
//Internals
import './index.css';

const Header = () => (
  <div className="header">
    <h1 id="header-title">Shop Now</h1>
    <table className="links-header">
      <tr>
        <td>
          <Link  className="nav-link-header" to="/women"><button type="button">Women</button></Link>
        </td>
        <td>
          <Link  className="nav-link-header" to="/men"><button type="button">Men</button></Link>
        </td>
        <td>
          <Link  className="nav-link-header" to="/clothes"><button type="button">Clothes</button></Link>
        </td>
        <td>
          <Link  className="nav-link-header" to="/accessories"><button>Accessories</button></Link>
        </td>
      </tr>

    </table>
  </div>
)

export default Header;
