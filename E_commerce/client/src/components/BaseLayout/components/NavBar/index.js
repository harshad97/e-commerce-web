// Dependencies
import React from "react";
import { Icon } from "react-materialize";
import { NavLink } from "react-router-dom";
// Internals
import "./index.css";


const Navbar = () => (
  <nav className="navbar">
    <div className="nav-links">
      <ul id="dropdown1" class="dropdown-content">
        <li>
          <NavLink
            activeClassName="selected"
            className="nav-link nav-tablet"
            exact
            to="/"
          >
            Home
          </NavLink>
        </li>
        <li>
          <NavLink
            activeClassName="selected"
            className="nav-link nav-tablet"
            to="/women"
          >
            Women
          </NavLink>
        </li>
        <li>
          <NavLink
            activeClassName="selected"
            className="nav-link nav-tablet"
            to="/men"
          >
            Men
          </NavLink>
        </li>
        <li>
          <NavLink activeClassName="selected" className="nav-link" to="/login">
            Login
          </NavLink>
        </li>
        <li>
          <NavLink
            activeClassName="selected"
            className="nav-link"
            to="/register"
          >
            Register
          </NavLink>
        </li>
        
      </ul>
    </div>

    <div className="shopping-cart">
      <NavLink to="/cart">
      <Icon medium>shopping_cart</Icon>
      </NavLink>
    </div>
  </nav>
);

export default Navbar;
