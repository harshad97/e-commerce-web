import React, { Component } from "react";
import "./style.css";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      valid: false,
    };
  }
  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };
  handleSubmit = (e) => {
    e.preventDefault();
    fetch('http://localhost:4000/user/userlogin',{
      method: "POST", 
      body: JSON.stringify({
        email:this.state.email,
        password: this.state.password
      }),
      headers: { 
        "Content-type": "application/json; charset=UTF-8"
    } 
    })
    .then(response => response.json())
    .then(json => {
      if(json.success == true){
        localStorage.setItem("auth","true");
        localStorage.setItem("name",json.user.UId);
        this.props.history.push('/')  
      }else{
         this.setState({
            valid : true 
         }) 
         setTimeout(() => {
          this.setState({
            valid :false 
          })
         },2000 )
      }
    });
  };
  render() {
    const { email, password, valid } = this.state;
    const {auth} = this.props
    return (
      <div className="row">
        <p> {auth} </p>
        <h3 className="auth-heading"> Login </h3>
        <form className="col s12" onSubmit={this.handleSubmit}>
          <div className="row">
            {valid && <p id="error-login"> invalid username or password </p>}
            <div className="input-field col s12">
              <input
                id="email"
                name="email"
                type="email"
                value={email}
                className="validate"
                onChange={this.handleChange}
                required
              />
              <label for="email">Email</label>
            </div>
            <div className="input-field col s12">
              <input
                id="password"
                name="password"
                value={password}
                type="password"
                onChange={this.handleChange}
                className="validate"
                required
              />
              <label for="password">Password</label>
            </div>
            <div className="input-field col s12">
              <button type="submit" id="login">
                Login
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
  componentDidMount() {
    window.scrollTo(0,document.body.scrollHeight);
  }
}
export default Login;
