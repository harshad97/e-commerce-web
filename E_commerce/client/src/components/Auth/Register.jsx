import React, { Component } from "react";
import "./style.css";

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displayName:"",  
      email: "",
      password: "",
    };
  }
  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };
  handleSubmit = (e) => {
    e.preventDefault();
    fetch('http://localhost:4000/user/adduser',{
      method: "POST", 
      body: JSON.stringify({
        displayName : this.state.displayName,
        email:this.state.email,
        password: this.state.password
      }),
      headers: { 
        "Content-type": "application/json; charset=UTF-8"
    } 
    })
    .then(response => response.json())
    .then(json => {
        console.log(json)
        if(json.success == true){
          localStorage.setItem("auth","true");
          localStorage.setItem("name",json.result.UId);
          this.props.history.push('/')
        }else{
          console.log(json)
        }
    })
  };
  render() {
    const { email, password,displayName } = this.state;
    return (
      <div className="row">
        <h3 className="auth-heading"> Register </h3>
        <form className="col s12" onSubmit={this.handleSubmit}>
          <div className="row">
            <div className="input-field col s12">
              <input
                id="displayName"
                name="displayName"
                type="text"
                value={displayName}
                className="validate"
                onChange={this.handleChange}
                required
              />
              <label for="displayName">Display Name</label>
            </div>
            <div className="input-field col s12">
              <input
                id="email"
                name="email"
                type="email"
                value={email}
                className="validate"
                onChange={this.handleChange}
                required
              />
              <label for="email">Email</label>
              
            </div>
            <div className="input-field col s12">
              <input
                id="password"
                name="password"
                value={password}
                type="password"
                onChange={this.handleChange}
                className="validate"
                required
              />
              <label for="password">Password</label>
            
            </div>
            <div className="input-field col s12">
              <button type="submit" id="login">
                Register
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
  componentDidMount() {
    window.scrollTo(0,document.body.scrollHeight);
  }
}
export default Register;
