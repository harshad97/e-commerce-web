const mongoose = require('mongoose')
const express = require('express')
const path = require('path')
const cors = require('cors')
const userRoute = require('./routes/user')
const categoryRoute = require('./routes/category')

const app = express()
app.use(express.json())
app.use(cors());

mongoose.connect('mongodb+srv://Sid:sid1234@ecommerce-mlacg.mongodb.net/test?retryWrites=true&w=majority')
    .then(result => {
        console.log('mongodb connected successfully')
    })
    .catch(error => {
        console.log('Connection failed')
    })

app.use('/user', userRoute)
app.use('/category', categoryRoute)

const port = process.env.PORT || 4000;

app.listen(4000,() =>{
    console.log(`Magic Happens At Port ${port}!!!!`)
})