const Category = require('../models/category')
const router = require('express').Router()

router.get('/getcategory', (req, res) => {
    Category.find()
        .then(result => {
            res.json(result)
        })
        .catch(error => {
            console.log(error)
            res.json('An Error Occured')
        })
})

router.post('/addcategory', (req, res) => {
    Category.insertMany([
      ])
      .then(result => {
          console.log(result)
      })
      .catch(error => {
          console.log(error)
      })
})

module.exports = router