const User = require('../models/user')
const router = require('express').Router()

//get all users
router.get('/user', (req, res) => {
    User.find()
        .then(result => {
            res.json(result)
        })
        .catch(error => {
            console.log(error)
            res.json('No users found')
        })
})

//add user
router.post('/adduser', (req, res) => {
    console.log(req.body)
    const newUser = new User ({
        UId: req.body.displayName,
        UEmail: req.body.email,
        UPwd: req.body.password
    })

    newUser.save()
        .then(result => {
            res.json({result, success:true})
        })
        .catch(error => {
            res.json({error : 'User Creation Filed',success:false})
            console.log(error)
        })
})

//user login
router.post('/userlogin', (req, res) => {
    User.findOne({UEmail: req.body.email})
        .then(user => {
            if(user){
                if(user.UPwd === req.body.password){
                    res.json({user,success:true})
                } else {
                    res.json({error :'Invalid Password',success:false})
                }
            } else {
                res.json('User Not Found')
            }
        })
        .catch(error => {
            res.json('Error occured while login')
            console.log(errors)
        })
})

module.exports = router