const mongoose = require('mongoose')
const UserSchema = mongoose.Schema({
    UId: {
        type:String,
        require: true
    },
    UEmail:{
        type: String,
        require: true
    },
    UPwd: {
        type: String,
        require: true
    }
})

module.exports = mongoose.model('User', UserSchema)