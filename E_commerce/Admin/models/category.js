const mongoose = require('mongoose')
const CategorySchema = mongoose.Schema({
    name:{
        type: String,
        required: true
    },
    description:{
        type:String,
        required: true
    },
    price:{
        type: Number,
        required: true
    },
    gender:{
        type: String,
        required: true
    },
    type:{
        type:String,
        required: true
    },
    img:{
        type: String,
        required: true
    },
    inCart: {
        type: Boolean,
        default: false
    },
    category:{
        type: String,
        required: true
    }
})

module.exports = mongoose.model('Category', CategorySchema)